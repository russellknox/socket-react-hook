### React Sockets using hooks

This library uses context to pass ```message, send, error, close, loading``` to any children components who subscribe.

```
import {Socket} from './Socket'

<Socket
 url="ws://..."
>
    <Child>
</Socket>

// Child
import { SocketContext } from '../socketContext'

const Child = () => {
  const { message, send, error, close, loading } = useContext(SocketContext)
}
```

To create a socket just pass in a url. The socket also accepts the following props:

- `url`: Socket string
- `initialState`: defaults to [],
- `onMessage`: fired when a new message is received. Func recevies state and ws message

```
const onMessage = (state, message) => [...state, message]
const onMessage = (state = {}, message) => {...state, message}

```
- `onOpen`: fired when socket opens
- `onClose`: fired when socket closes
- `onError`: fired when socket errors
- `onUnmount`: fired when socket unmount
- `beforeConnection`: Async func fired before socket connection is made, Func is passed url string, where you can add auth if required

```
const beforeConnectio = async url => {
    ...something async
    return url + '?username=userna,e'
}
```

##### Socket context
 - `data`: the current state. This is NOT the message from the socket. This is current state. By default this is an array, and each message is appended. You can change this default with the custom onMessage prop
- `error`: boolean to indicate when the socket has errored on the server
- `close`: boolean to indicate when the socket has closed on the server
- `send`: input will be JSON.stringify() and sent to server

