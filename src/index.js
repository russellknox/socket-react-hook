import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './example/Socket';

ReactDOM.render(<App />, document.getElementById('root'));
