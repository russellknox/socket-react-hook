import React from 'react'

const SocketContext = React.createContext()

const {Provider, Consumer} = SocketContext

export {Provider, SocketContext, Consumer}