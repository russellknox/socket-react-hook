import {setOnMessage, setOnError, setOnClose} from './events'

it('should call setOnError with the curried err', () => {
    //export const setOnError = x => y => x(y)
    const errorResponse = 'ERROR'
    const setError = jest.fn()
    const onError = jest.fn(() => errorResponse)

    // will come from Socket onerror event
    const socketError = false

    setOnError(setError, onError)(socketError)

    expect(setError).toHaveBeenCalledWith(errorResponse)
    expect(onError).toHaveBeenCalledWith(socketError)
})

it('should call setOnClose with updates setClose state hook with custom onClose', () => {
    //export const setOnClose = x => y => x(y)
    const closeResponse = 123
    const setClose = jest.fn()
    const onClose = jest.fn(() => closeResponse)

    // will come from Socket onclose event
    const socketClose = true

    setOnClose(setClose, onClose)(socketClose)

    expect(setClose).toHaveBeenCalledWith(closeResponse)
    expect(onClose).toHaveBeenCalledWith(socketClose)

})

it('should call setOnMessage with a new message and custom onMessage', () => {
    // export const onMessage = set => ({data}) => set(prev => [...prev, data])
    const data = 1234
    const state = {message: 1234}
    const onMessage = jest.fn((a, b) => [a, b])
    const setMessage = jest.fn((a) => onMessage(1, data))

    setOnMessage(setMessage, onMessage)({data})
    
    expect(setMessage).toHaveBeenCalledWith(expect.any(Function))
    expect(onMessage).toHaveBeenCalledWith(1, data)
})