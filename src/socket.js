import React, { useState, useEffect } from 'react'
import propType from 'prop-types'
import { Provider } from './socketContext'
import { setOnMessage, setOnClose, setOnError, setOnOpen, onSend, setOnUnmount } from './events'
import { createSocketEvent, removeSocketEvent, handleBeforeConnection } from './helpers'

const Socket = ({
  url,
  children,
  initialState = [],
  onMessage = (state, message) => [...state, message],
  onClose = close => close,
  onError = error => error,
  onOpen = socket => socket,
  onUnmount = _ => {},
  beforeConnection = false,
}) => {
  const [socket, setSocket] = useState()
  const [message, setMessage] = useState(initialState)
  const [close, setClose] = useState(false)
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(true)
  const [mounted, setMounted] = useState(false)

  // allow promise to abort during the socket setup and before connection
  const abortController = new AbortController()
  const signal = abortController.signal

  const handleSetup = async () => {
    try {
      const connection = beforeConnection ? await handleBeforeConnection(beforeConnection, url, signal) : url
      return setUpSockets(connection)
    } catch (error) {
      return Promise.reject({ error })
    }
  }

  useEffect(() => {
    // setup will return a promise
    const setup = handleSetup().catch(error => {
      // This catch block is for applicaion rejections
      // If abortController rejection, return error to unmounting catch below to handle
      if (signal.aborted) return error
      setLoading(false)
      setError(error)
    })

    return () => {
      // when returing from useEffect, if setup promise has not resolved i.e. beforeConnection func still pending
      // when components unmounts, listeners are not destroyed and setState will still be called when promise resovles
      // If component unmounts before beforeConnection promise has resolve, beforeConnection promise
      // will be aborted and will throw an error. No listeners would be setup so will not need removing
      abortController.abort()
      setup.then(socket => socket.destroy()).catch(error => error)
    }
  }, [])

  function setUpSockets(connection) {
    // use window.WebSocket as erroring because of dev server also uses WebSocket
    const socket = new window.WebSocket(connection)
    setSocket(socket)
    const on = createSocketEvent(socket)
    const remove = removeSocketEvent(socket)

    on('open', setOnOpen(onOpen, setLoading, setMounted))
    on('message', setOnMessage(setMessage, onMessage))
    on('error', setOnError(setError, onError))
    on('close', setOnClose(setClose, onClose))

    function destroy() {
      remove('open', setOnOpen(onOpen, setLoading, setMounted))
      remove('message', setOnMessage(setMessage, onMessage))
      remove('error', setOnError(setError, onError))
      remove('close', setOnClose(setClose, onClose))

      setOnUnmount(socket, onUnmount)
    }

    return {
      destroy,
    }
  }
  return (
    <Provider
      value={{
        send: onSend(socket),
        message,
        error,
        close,
        loading,
        open: mounted,
      }}
    >
      {/* only return children once connection has been open. Loading prop is exposed to allow consumer to show laoding */}
      {children}
    </Provider>
  )
}

WebSocket.propTypes = {
  url: propType.string.isRequired,
  initialState: propType.oneOf([propType.array, propType.object, propType.string]),
  onMessage: propType.func,
  onOpen: propType.func,
  onClose: propType.func,
  onError: propType.func,
  onUnmount: propType.func,
  children: propType.children,
  beforeConnection: propType.func,
}

export { Socket }
