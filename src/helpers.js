export const createSocketEvent = socket => (type, callback) => socket.addEventListener(type, callback)

export const removeSocketEvent = socket => (type, callback) => socket.removeEventListener(type, callback)

export const handleBeforeConnection = (func, params, signal) => {
  return new Promise((resolve, reject) => {
    signal.addEventListener('abort', () => {
      return reject()
    })
    func(params)
      .then(url => resolve(url))
      .catch(error => {
        reject('Error connecting to server')
      })
  })
}
