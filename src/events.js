// events
export const setOnMessage = (setMessage, onMessage) =>
  // "data" comes from the socket onmessage event
  ({ data }) => setMessage(state => onMessage(state, data))

export const setOnClose = (setClose, onClose) => close => setClose(onClose(close))

export const setOnError = (setError, onError) =>
  // "error" comes from socket onclose event
  error => setError(onError(error))

export const setOnOpen = (onOpen, setLoading, setMounted) =>
  // "data" is event
  data => {
    // set loading to false once the socket has connected and has opened
    setMounted(true)
    setLoading(false)
    onOpen(data)
  }

// methods
export const onSend = socket => message => socket.send(message)

export const setOnUnmount = (socket, onUnmount) => {
  onUnmount()
}
