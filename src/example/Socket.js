import React, { PureComponent } from 'react'
import { Socket } from '../socket'
import { PageOne, PageTwo } from './Page'

const onMessage = (state, data) => (state = data)
const onError = params => console.log(`Error recevied with params: ${params}`)
const onClose = params => console.log(`Close recevied with params: ${params}`)

class App extends PureComponent {
  state = {
    hide: false,
  }

  hide = () => this.setState({ hide: !this.state.hide })

  render() {
    return (
      <div>
        <>
          {this.state.hide && (
            <div>
              <div>Socket 1: Message overwrites state and renders when onOpen event is fired</div>
              <Socket
                url="wss://echo.websocket.org"
                onMessage={(state, data) => (state = data)}
                onError={onError}
                onClose={onClose}
                initialState={''}
              >
                <PageOne />
              </Socket>
              -------------------------------------------
              <div>Socket 2: Message gets added to state and render async after 3000ms</div>
              <Socket
                url="wss://echo.websocket.org"
                onMessage={(state, data) => [...state, data]}
                onUnmount={() => alert('Socket two closed')}
                onError={onError}
                onClose={onClose}
                beforeConnection={beforeConnection(2000)}
              >
                <PageTwo />
              </Socket>
            </div>
          )}
        </>
        <button onClick={this.hide}>{this.state.hide ? <div>Hide Sockets</div> : <div>Show Sockets</div>}</button>
      </div>
    )
  }
}

function beforeConnection(duration) {
  return url =>
    new Promise((res, rej) => {
      setTimeout(() => res(url), duration)
    })
}

export { App }
