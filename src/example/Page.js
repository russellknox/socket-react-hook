import React, { useContext, useState } from 'react'
import { SocketContext } from '../socketContext'

const PageOne = () => {
  const { message, send, error, close, loading } = useContext(SocketContext)
  const [text, setText] = useState()

  if (loading) return <div>Loading...</div>
  if (error) return <div>{error.error}</div>

  return (
    <div>
      <h1>First Socket</h1>
      <input placeholder="Send Message" onChange={e => setText(e.target.value)} />
      <button onClick={() => send(text)}>SEND</button>
      {message ? <div>New message: {message}</div> : <div>No messages recevied. Type a message and send button</div>}
      {error ? <div>{error.error}</div> : null}
      {close ? <div>Closed</div> : null}
    </div>
  )
}

const PageTwo = () => {
  const { message, send, error, close, loading } = useContext(SocketContext)
  const [text, setText] = useState()

  if (loading) return <div>Loading...</div>
  if (error) return <div>{error.error}</div>

  return (
    <div>
      <h1>Second Socket</h1>
      <input placeholder="Send Message" onChange={e => setText(e.target.value)} />
      <button onClick={() => send(text)}>SEND</button>
      {message.length === 0 ? (
        <div>No messages recevied. Type a message and send button</div>
      ) : (
        <>
          <h3>Messages</h3>
          <ul>
            {message.map((message, i) => (
              <li key={i}>{message}</li>
            ))}
          </ul>
        </>
      )}
      {error ? <div>Error</div> : null}
      {close ? <div>Closed</div> : null}
    </div>
  )
}

export { PageOne, PageTwo }
