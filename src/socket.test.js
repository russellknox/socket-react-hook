import React from 'react'
import { WebSocket as TestWebSocket, Server } from 'mock-socket'
import { render, cleanup, wait, flushEffects } from 'react-testing-library'
import { Consumer } from './socketContext'
import { Socket } from './Socket'
import * as event from './events'
import * as helpers from './helpers'

jest.useFakeTimers()

const Child = ({ error, send, message, close }) => {
  return (
    <div className="test">
      <div data-testid="send" onClick={() => send('hello')} />
      {message && <div data-testid="data">{message}</div>}
      {error && <div data-testid="error">ERROR MESSAGE</div>}
      {close && <div data-testid="close">CLOSED MESSAGE</div>}
    </div>
  )
}
const setUpTests = props => (
  <Socket {...props}>
    <Consumer>{props => <Child {...props} />}</Consumer>
  </Socket>
)

beforeEach(() => (global.WebSocket = TestWebSocket))
afterEach(cleanup)

it('Message is updated when message is recevied from the server', () => {
  const url = 'ws://localhost:8080'
  const onMessage = jest.fn((statem, data) => [...statem, data])
  const mockServer = new Server(url)
  mockServer.on('connection', socket => socket.send('From the Mock Server'))

  const { queryByTestId } = render(setUpTests({ url, onMessage }))

  jest.runAllTimers()

  expect(onMessage).toHaveBeenCalled()
  expect(queryByTestId('data').textContent).toBe('From the Mock Server')
  expect(queryByTestId('error')).toBe(null)
  expect(queryByTestId('close')).toBe(null)
})

it('Close event is recevied in Child components and renders close message', () => {
  const url = 'ws://localhost:8081'
  const onClose = jest.fn(close => close)
  const mockServer = new Server(url)
  mockServer.on('connection', socket => socket.close())

  const { queryByTestId } = render(setUpTests({ url, onClose }))

  jest.runAllTimers()
  expect(onClose).toHaveBeenCalled()
  expect(queryByTestId('close').textContent).toBe('CLOSED MESSAGE')
  expect(queryByTestId('error')).toBe(null)
  expect(queryByTestId('message')).toBe(null)
})

it('Error event is recevied in Child components and renders error message and calls onError', () => {
  const url = 'ws://localhost:8082'
  const onError = jest.fn(error => error)
  const mockServer = new Server(url)
  mockServer.on('connection', socket => socket.listeners.error[0](new Error()))

  const { queryByTestId } = render(setUpTests({ url, onError }))

  jest.runAllTimers()
  expect(onError).toHaveBeenCalled()
  expect(queryByTestId('error').textContent).toBe('ERROR MESSAGE')
  expect(queryByTestId('message')).toBe(null)
})

it('setOnUnmount to be called when component un-mounts', async () => {
  jest.spyOn(event, 'setOnUnmount')
  const onUnmount = jest.fn()
  const url = 'ws://localhost:8088'
  new Server(url)
  const { unmount, rerender } = render(setUpTests({ url, onUnmount }))
  await rerender()
  await unmount()
  expect(event.setOnUnmount).toHaveBeenCalled()
  expect(onUnmount).toHaveBeenCalled()
})

it('beforeConnection to be called when component mounts', () => {
  const beforeConnection = jest.fn(async a => a)
  const url = 'ws://localhost:8089'
  const { rerender } = render(setUpTests({ url, beforeConnection }))
  rerender()
  expect(beforeConnection).toHaveBeenCalled()
})

it('Error returned if before beforeConnection promise throws a rejection', async () => {
  const beforeConnection = jest.fn(async a => a)
  const url = 'ws://localhost:8044'
  jest.spyOn(helpers, 'handleBeforeConnection')
  await helpers.handleBeforeConnection.mockRejectedValue('sdf')
  const { queryByTestId } = render(setUpTests({ url, beforeConnection }))
  await flushEffects()
  await wait(() => queryByTestId('error'))
  await expect(queryByTestId('error').textContent).toBe('ERROR MESSAGE')
})
